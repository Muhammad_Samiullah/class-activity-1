package com.example.classactivity1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    public void OnclickButtonDefaultToast(View view) {
        Toast.makeText(this, "Hello World", Toast.LENGTH_SHORT).show();
    }

    public void OnclickButtonCustomizedToast(View view) {
        View toast = LayoutInflater.from(this).inflate(R.layout.custom_toast, null);
        Toast custom = new Toast(this);
        custom.setView(toast);
        TextView message = toast.findViewById(R.id.tvToastMessage);
        message.setText("      Hello SMD       ");
        custom.setDuration(Toast.LENGTH_SHORT);
        custom.show();
    }
}